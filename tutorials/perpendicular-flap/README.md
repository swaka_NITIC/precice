# perpendicular-flap

チュートリアルをgitでcloneしてきます。
沢山のテストケースが含まれますが、ここではFSIの実行テストとして"perpendicular-flap"を実行してみます。  

これは流路の中に、柔軟な構造体(flap)が存在し、流体力によって変形・移動し、その結果、流れ場が変わるというFSIの典型例です。

<img src="https://github.com/precice/tutorials/blob/master/perpendicular-flap/images/tutorials-perpendicular-flap-physics.png?raw=true">

## tutorialの入手

```
git clone https://github.com/precice/tutorials.git
cd tutorials
cd perpendicular-flap
```

## perpendicular-flapフォルダの中身

ごちゃごちゃしていますが、今回のOpenFOAM+CalculixでのFSI計算に必要なものは、fluid-openfoamとsolid-calculixの２つのフォルダと、preciseの設定を記述した"precice-config.xml" だけです。このxmlファイルについては別途、調べて解説します。  
それ以外は、プロット用、クリーンアップ用スクリプトと、今回導入していない他のソルバー用フォルダになります（使いません）。

## 解析の実行

ターミナルを２つ起動してください。  
それぞれのターミナルで、以下のように上記フォルダに含まれるrun.shを実行します。
相互のプロセス間でpreCICEライブラリを介して計算データ（力や変位）をやり取りしながら計算を進めますので、途中で計算が止まる場合はどちらかに問題があると考えられます。

```
cd fluid-openfoam
./run.sh
```

```
cd solid-calculix
./run.sh
```

## 計算結果の確認

２次元計算をしていますが、流れに垂直に柔軟な梁が置かれ（根本で拘束）、流れで変形・振動していることが分かります。
今回の可視化は、fluid-openfoam内の流体解析結果をparaviewで読み込みました。

<img src="./anim.gif">

solid側の可視化を行いたい場合は、solid-calculixフォルダ中のflap.frdが計算結果になります。OpenFOAMと異なり、一つのファイルに全ての時刻の結果が出力されています。
これをparaviewで読み込めるvtk形式に変換する必要がありますが、変換ツールが付属してますのでビルドして使いましょう。

```
cd calculix-adapter/tools/frdConverter (README.mdにビルドの手順が記載されてます)

sudo apt update
sudo apt install mono-complete
mcs -out:frdToVTKConverter.exe frdToVTKConverter.cs
mono frdToVTKConverter.exe /path/to/frdFile.frd  (monoでC#プログラムを起動しています)
```

具体的にfrdを変換するには、perpendicular-flap/solid-caclulixフォルダにビルドしたfrdToVTKConverter.exeをコピーして、以下のようにvtkファイルを出力します。
あとはこれらのvtkファイルをparaviewで読み込みます。

```
mono frdToVTKConverter.exe ./flap.frd

: (ERRORとか言ってきますが)
Enter the number of Timesteps  （変換する時間ステップの数を入力します）
50
: Writing new file: ./flap_mmh_0.vtk
: Writing new file: ./flap_mmh_1.vtk
[snip]
: Writing new file: ./flap_mmh_47.vtk
: Writing new file: ./flap_mmh_48.vtk
: Writing new file: ./flap_mmh_49.vtk
```

<img src="./flap.png" size=50%>
