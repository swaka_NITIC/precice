# preCICEのセットアップの方法

tutorial/perpendicular-flapを題材に、 precice-config.xmlファイルや、カップリングするFluid,Solidのケースフォルダについての設定についてまとめます。

## ケースフォルダの構成

tutorialのケースはpreCICE付属のツールスクリプトを想定しているので、ローカルで実行できるように使用しているスクリプトをコピーし、perpendicular-flapの[最小構成版flap2D](./flap2D.tar.gz)を作ってみました。

openfoamとcalculixでのFSIカップリングです。fluid-openfoamはOpenFOAMのケースフォルダ、solid-calclixは、CAE解析に必要なinpファイルやmshファイルを格納しています。
#1～#3を順番に確認していきます。

```
.
├── cleaning-tools.sh
├── fluid-openfoam/  (#3)
├── plot-all-displacements.sh
├── plot-displacement.sh
├── precice-config.xml　(#1)
└── solid-calculix/ (#2)
```

## (1) precice-config.xml

preciceのカップリングの計算条件を設定するxmlファイルで、ファイル名は決め打ちです。
設定の詳細については、[こちら](https://precice.org/configuration-xml-reference.html)を参考に。

```
<?xml version="1.0" encoding="UTF-8" ?>
<precice-configuration>
  <log>  (log出力の指定。必須ではない)
    <sink
      filter="%Severity% > debug and %Rank% = 0"
      format="---[precice] %ColorizedSeverity% %Message%"
      enabled="true" />
  </log>

  <solver-interface dimensions="2">　 (次元の指定)
    <data:vector name="Force" />　　　　（カップリングデータの列挙）
    <data:vector name="Displacement" />　　

    <mesh name="Fluid-Mesh">　（ソルバー間の境界メッシュを指定；個々のソルバーの設定での名前と一致させること）
      <use-data name="Force" />
      <use-data name="Displacement" />
    </mesh>

    <mesh name="Solid-Mesh">　（　同上　）
      <use-data name="Displacement" />
      <use-data name="Force" />
    </mesh>

    <participant name="Fluid">　　　（マッピングをする2つのメッシュ、マッピング方法等を設定）
      <use-mesh name="Fluid-Mesh" provide="yes" />
      <use-mesh name="Solid-Mesh" from="Solid" />
      <write-data name="Force" mesh="Fluid-Mesh" />
      <read-data name="Displacement" mesh="Fluid-Mesh" />
      <mapping:rbf-thin-plate-splines
        direction="write"
        from="Fluid-Mesh"
        to="Solid-Mesh"
        constraint="conservative" />
      <mapping:rbf-thin-plate-splines
        direction="read"
        from="Solid-Mesh"
        to="Fluid-Mesh"
        constraint="consistent" />
    </participant>

    <participant name="Solid">　　　　（ 同上 ）
      <use-mesh name="Solid-Mesh" provide="yes" />
      <write-data name="Displacement" mesh="Solid-Mesh" />
      <read-data name="Force" mesh="Solid-Mesh" />
      <watch-point mesh="Solid-Mesh" name="Flap-Tip" coordinate="0.0;1" />
    </participant>

    <m2n:sockets from="Fluid" to="Solid" exchange-directory=".." />　　（participants館のデータ交換）

    <coupling-scheme:parallel-implicit>　　（カップリングスキーム指定：重要）
      <time-window-size value="0.01" />
      <max-time value="5" />
      <participants first="Fluid" second="Solid" />
      <exchange data="Force" mesh="Solid-Mesh" from="Fluid" to="Solid" />
      <exchange data="Displacement" mesh="Solid-Mesh" from="Solid" to="Fluid" />
      <max-iterations value="50" />
      <relative-convergence-measure limit="5e-3" data="Displacement" mesh="Solid-Mesh" />
      <relative-convergence-measure limit="5e-3" data="Force" mesh="Solid-Mesh" />
      <acceleration:IQN-ILS>
        <data name="Displacement" mesh="Solid-Mesh" />
        <data name="Force" mesh="Solid-Mesh" />
        <preconditioner type="residual-sum" />
        <filter type="QR2" limit="1e-2" />
        <initial-relaxation value="0.5" />
        <max-used-iterations value="100" />
        <time-windows-reused value="15" />
      </acceleration:IQN-ILS>
    </coupling-scheme:parallel-implicit>
  </solver-interface>
</precice-configuration>
```

## (2) solid-calclix

フォルダ構成

```
.
├── WarnNodeMissMultiStage.nam
├── all.msh　　　　　　　（メッシュデータ:flap.inpからinclude）
├── clean.sh
├── config.yml　　　　　（preCICEの設定ymlファイル）
├── fix1_beam.nam　　　（拘束条件fix1のノードセットデータ：flap.inpからinclude）
├── flap.inp　　　　　　（calculixのinpファイル）　　
├── interface_beam.nam （荷重条件interfaceのノードセットデータ：flap.inpからinclude）
└── run.sh　　　　　　　（実行スクリプト）　　
```

flap.inp
```
（メッシュデータの読み込み）
*INCLUDE, INPUT=all.msh
*INCLUDE, INPUT=fix1_beam.nam
*INCLUDE, INPUT=interface_beam.nam

（材料特性の作成：名前・ヤング率・ポアソン比・密度）
*MATERIAL, Name=EL
*ELASTIC
 4000000, 0.3
*DENSITY
 3000

（エレメントセットに対する材料の指定）
*SOLID SECTION, Elset=Eall, Material=EL

（解析ステップの設定: *STEP～*END STEP）
*STEP, INC=1000000
*DYNAMIC, ALPHA=0.0, DIRECT (see； https://www.xsim.info/articles/CalculiX/ccx-doc/node222.html?msclkid=560c3066b7fc11ecb6a2329b17ae52c1)
1.E-2, 5.0
*BOUNDARY　（see; https://www.xsim.info/articles/CalculiX/ccx-doc/node186.html?msclkid=07f1246ab7fd11ec9cabd494a527b971）
Nfix1, 1, 3   (B.C. fix1に)
*BOUNDARY
Nall, 3
*CLOAD　（負荷　ここでは仮にすべての方向に0 N）
 Nsurface, 1, 0.0
 Nsurface, 2, 0.0
 Nsurface, 3, 0.0
*NODE FILE　（ノードでのfile出力データの指定）
 U
*EL FILE  （エレメントのfile出力データの指定）
 S, E
*END STEP

```

config.yml
```
participants:  （participantsとしての登録）

    Solid:
        interfaces:
        - nodes-mesh: Solid-Mesh  　 （登録名）
          patch: surface 　　　　　  （パッチ名：interface_beam.namで指定したNode set名surfaceを指定）
          read-data: [Force]　　　　　（流体側からForceを読む）
          write-data: [Displacement]　（個体側から変位を出力）

precice-config-file: ../precice-config.xml  （precice-config.xmlへのパス指定）
```


## (3) fluid-openfoam

```
.
├── 0
│   ├── U
│   ├── p
│   ├── phi
│   └── pointDisplacement
├── clean.sh
├── constant
│   ├── dynamicMeshDict
│   ├── transportProperties
│   └── turbulenceProperties
├── openfoam-remove-empty-dirs.sh
├── run-openfoam.sh
├── run.sh
└── system
    ├── blockMeshDict
    ├── controlDict  *
    ├── decomposeParDict
    ├── fvSchemes
    ├── fvSolution
    └── preciceDict　*
```

OpenFOAMのpimpleFoamソルバーを用いるケースフォルダになっています。初期値から10sec.計算する非定常層流ケースです。
ちなみに、FSIでの流体は圧縮性、非圧縮性両サポートとのことです。

流路はinlet, outlet, upperWall/lowerWallのBCで囲まれ、流路中央にflapが存在します。

流体力によりflapが変形し、constant/dynamicMeshDictで指定されているように流路メッシュがdisplacementLaplacianで変形する設定ですので、0フォルダにはpointDisplacementが含まれています。

設定については[こちら](https://precice.org/adapter-openfoam-config.html)にまとめられています。

```
constant/dynamicMeshDict：

FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      dynamicMeshDict;
}

dynamicFvMesh dynamicMotionSolverFvMesh;

motionSolverLibs ("libfvMotionSolvers.so");

solver      displacementLaplacian;

displacementLaplacianCoeffs {
    diffusivity quadratic inverseDistance (flap);
}
```

```
0/pointDisplacement：

FoamFile
{
    version     2.0;
    format      ascii;
    class       pointVectorField;
    object      pointDisplacement;
}

dimensions      [0 1 0 0 0 0 0];

internalField   uniform (0 0 0);

boundaryField
{
    inlet
    {
        type            fixedValue;
        value           uniform (0 0 0);
    }

    outlet
    {
        type            fixedValue;
        value           uniform (0 0 0);
    }

    flap
    {
        type            fixedValue;
        value           $internalField;
    }

    upperWall
    {
        type            slip;
    }

    lowerWall
    {
        type            slip;
    }

    frontAndBack
    {
        type            empty;
    }
}
```

通常のpimpleFoamケースと異なる点は以下の通りです。

1. system/controlDictにopenfoam-adapterのライブラリを読み込む設定を記述する。

```
system/controlDict:

FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      controlDict;
}

application     pimpleFoam;       // latest OpenFOAM
// application     pimpleDyMFoam; // OpenFOAM v1712, OpenFOAM 5.x, or older
:
[snip]
:
functions
{
    preCICE_Adapter (←この指定は任意名称でOK)
    {
        type preciceAdapterFunctionObject;
        libs ("libpreciceAdapterFunctionObject.so");
    }
}
```

2. system/preciceDictを作成する。

```
system/preciceDict:

FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      preciceDict;
}

preciceConfig "../precice-config.xml";  （xmlファイルの指定）

participant Fluid;　　（participant名の指定。precice-config.xmlでも指定する）

modules (FSI);　　(moduleの指定、CHT or/and FSI；スペース空けて二つ指定も可能)

interfaces　　（カップリングのinterface指定セクション）
{
  Interface1　　（interface名；ここではこの１つだけ）
  {
    mesh              Fluid-Mesh;  （メッシュ名の指定；precice-config.xmlでも指定する)
    patches           (flap);    (OFのパッチ名)
    locations         faceCenters;　　（物理量の定義点指定）

    readData　　（このinterfaceでカップリング相手から読み取るデータ）
    (
      Displacement  (変位vector)
    );

    writeData　　（このinterfaceでカップリング相手に出力するデータ）
    (
      Force    (力vector)
    );
  };
};

FSI　　（FSIで非圧縮計算の、nuやrhoを指定することも可能）
{
  solverType  incompressible;
  nu   nu  [ 0 2 -1 0 0 0 0 ] 1.5e-5;
  rho rho [1 -3 0 0 0 0 0] 1.2;
}
```


