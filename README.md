# [preCICE](https://precice.org/)

preCICEは、多様なソルバーをカップリングさせるオープンソースライブラリです。2022年4月現在でver.2.3.0です。  
例えばOpenFOAM(CFD)と、Calculix(FEA）を連携させた流体構造連成解析：FSI（Fluid Structure Interaction)などが実現できます。  

カップリングのためのライブラリ本体以外に、組み合わせるソルバー毎にadapterと呼ばれるライブラリも必要になり、ソルバー毎に開発が行われています。ユーザーコミュニティによって開発されているThird-party adapterも複数あります。サポートしているadapterについては[こちら](https://precice.org/adapters-overview.html)をご確認ください。  

本ページは、流体解析ソルバーにOpenFOAM、応力解析ソルバーにCalculixの利用を想定し、FSIを実行することを目指し、preCICEの導入（環境構築）と活用について説明していく資料になります。

なお、実行環境としては、WSL2 Ubuntu 20.04LTSの上に構築していきます。
OpenFOAM（OpenFOAM-v2112）の実行環境はすでに構築済みであるとします。

Ref) H.-J. Bungartz, F. Lindner, B. Gatzhammer, M. Mehl, K. Scheufele, A. Shukaev, and B. Uekermann: preCICE - A Fully Parallel Library for Multi-Physics Surface Coupling. Computers and Fluids, 141, 250–258, 2016.

## preCICEライブラリの導入

preCICEライブラリは、ビルド済みのバイナリパッケージで導入することも可能ですし、ソースからビルドすることも可能です。両方説明します。

### (お手軽な方法)バイナリパッケージでのインストール(Ubuntu 20.04LTS focal)

```
wget https://github.com/precice/precice/releases/download/v2.3.0/libprecice2_2.3.0_focal.deb
sudo apt install ./libprecice2_2.3.0_focal.deb
```

### （ちゃんと把握したい人向けの方法）ソースからのビルド・インストール(Ubuntu 20.04LS focal)

1. ソースコードの入手

```
git clone  https://github.com/precice/precice.git
```

2. 依存ライブラリのパッケージインストール

```
sudo apt update
sudo apt install build-essential cmake libeigen3-dev libxml2-dev libboost-all-dev petsc-dev python3-dev python3-numpy
```

3. Build Configuration and build

CMake option　は[こちら](https://precice.org/installation-source-configuration.html)を確認してください。下記は、全てdefault optionでのCMakeです。

```
cd precice
mkdir build
cd build
cmake .. 
make -j
```

4. テストの実行
buildディレクトリで、ctestコマンドを実行します。

```
ctest

:
35/38 Test #35: precice.tools.dtd .........................   Passed    0.07 sec
      Start 36: precice.tools.check.file
36/38 Test #36: precice.tools.check.file ..................   Passed    0.07 sec
      Start 37: precice.tools.check.file+name
37/38 Test #37: precice.tools.check.file+name .............   Passed    0.08 sec
      Start 38: precice.tools.check.file+name+size
38/38 Test #38: precice.tools.check.file+name+size ........   Passed    0.08 sec

100% tests passed, 0 tests failed out of 38

Label Time Summary:
Solverdummy    =   4.80 sec*proc (9 tests)
bin            =   0.73 sec*proc (10 tests)
petsc          =   0.52 sec*proc (1 test)
tools          =   0.73 sec*proc (10 tests)
```

5. インストール（default設定では、/usr/local以下に）
同じくbuildディレクトリで、

```
sudo make install
```

インストール状況をテストします。

```
make test_install

:
1: DUMMY: Reading iteration checkpoint
1: preCICE: min iteration convergence measure: #it = 2 of 5, conv = false
1: preCICE: Time window completed
1: preCICE: iteration: 1 of 2, time-window: 3 of 2, time: 2, time-window-size: 1, max-timestep-length: 1, ongoing: no, time-window-complete: yes,
1: DUMMY: Advancing in time
1: preCICE: Synchronize participants and close communication channels
1: DUMMY: Closing C++ solver dummy...
1: Success!
1/1 Test #1: precice.solverdummy ..............   Passed    0.43 sec

100% tests passed, 0 tests failed out of 1

Total Test time (real) =   0.43 sec
Built target test_install
```

これでpreCICEの導入は完了しました。もしビルド時に失敗するなどの場合は、
こちらの[Troubleshooting](https://precice.org/installation-source-troubleshooting.html)も確認してみましょう。たいてい、pathやLD_LIBRARY_PATHの設定が不足しています。

### 参考
1. https://precice.org/installation-overview.html
2. https://qiita.com/takaf05/items/cfd7ad54179849905fe5
3. http://penguinitis.g1.xrea.com/study/preCICE/OpenFOAM_CalculiX_FSI/OpenFOAM_CalculiX_FSI.html

## adapterの導入

次に、ソルバーに対応したadapterの導入を行います。

### [openfoam-adapter](https://github.com/precice/openfoam-adapter)

OpenFOAM用adapterの導入を行いますが、OpenFOAMの環境は構築済みとします。 また、導入したOpenFOAMのバージョンに応じたopenfoam-adapterの入手・ビルドが必要になります。

今回はOF-v2112を用いますが、この場合はdefaultのdevelopブランチでよいです。もしFoundation版OF(OpenFOAM9やOpenFOAMdevなど)を使っている場合は、適当なbranchに切り替えてください。

```
cd .. 　　 (preCICEのbuild ディレクトリを出る)
mkdir adapters 　　（adapterのソース保存フォルダ作成）
cd adapters　　　（そこへの移動）
git clone https://github.com/precice/openfoam-adapter.git

cd openfoam-adaper
git checkout develop (ブランチの切り替え)
(openFOAMの環境変数を設定してあることを確認してから)
./Allwamke

:
Building with WMake (see the wmake.log log file)...

Compiling enabled on 8 cores
wmake libso (openfoam-adapter)
Everything looks fine in wmake.log.
Everything looks fine in ldd.log.
=== OK: Building completed successfully! ===
```

### [calculix-adapter](https://precice.org/adapter-calculix-overview.html)

FSIを実行するため、応力解析ソルバーのCalculix用adapterを導入します。
バイナリパッケージもありますが、ここではソースをgitで落としてきて、ビルド
を行います。

```
cd .. 　　（adaptersフォルダ直下に戻る）
git clone https://github.com/precice/calculix-adapter.git
cd calculix-adapter
```

adapterのビルドの際に、preCICEライブラリを組み込んだ[Calculix](http://www.dhondt.de/)ソルバーccx_preCICEもビルドしますので、あらかじめCalculixのソース(ver.2.19)と、それに必要なSPOOLES2.2, ARPACK, yaml-cppが必要になります。  

まず先に、SPOOLES2.2などのライブラリを、Ubuntuのパッケージからinstallします。

```
sudo apt install libarpack2-dev libspooles-dev libyaml-cpp-dev
```

次に、Calculix 2.19のソースをダウンロードします。

```
wget http://www.dhondt.de/ccx_2.19.src.tar.bz2
tar xvjf ccx_2.19.src.tar.bz2   (CalculiXというフォルダが作成されます)
```

calculix-adapterのMakefileを以下のように書き換えます。
Calculixのソースパスを指定している部分になりますので、上述のCalculixのソースが異なる場所にある場合は適宜変更してください。
今回は、calculix-adapterのフォルダ直下にCalculiXというソースフォルダがある設定になっています。

``` 
diff Makefile Makefile.orig
6c6
< CCX             = ./CalculiX/ccx_$(CCX_VERSION)/src
---
> CCX             = $(HOME)/CalculiX/ccx_$(CCX_VERSION)/src
```

ビルドします。ビルドが成功したら、bin/フォルダにccx_preCICEという実行ファイルができていることを確認し、パスの通った場所にこれをコピーします。
/usr/local/bin以下で良いと思います。

```
make -j
sudo cp bin/ccx_preCICE /usr/local/bin
```

## [FSIの実行テスト](./tutorials/perpendicular-flap/README.md)

## [preCICEのセットアップの基本](./preCICE_setup/README.md)

